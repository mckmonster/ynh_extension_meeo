<?php
namespace YesWiki\Meeo\Controller;

include_once "includes/email.inc.php";

include_once "includes/email.inc.php";

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use YesWiki\Core\ApiResponse;
use YesWiki\Core\Controller\AuthController;
use YesWiki\Core\YesWikiController;
use YesWiki\Meeo\Controle\Controle;
use YesWiki\Meeo\Service\AbsenceManager;
use YesWiki\Meeo\Service\EleveManager;
use YesWiki\Meeo\Service\NoteManager;

class ApiController extends YesWikiController
{
  private $redirectParams;

  public function __construct() {
    
  }
  
  /**
  * @Route("/api/meeo/eleves",methods={"POST"},options={"acl":{"public"}})
  */
  public function eleveUpdate(Request $request) {
    $params = $this->getService(ParameterBagInterface::class);
    $redirectParams = $params->get('meeo')['redirect'];

    $eleveController = $this->getService(EleveManager::class);
    $content = $request->getContent();

    parse_str($content, $parameters);

    $datas = [];
    foreach ($parameters as $cle => $valeur) {
      $clef_action = explode('_', $cle);
      if ($clef_action[1] == 'classe' && $valeur != 'aucune') {
        $eleve = $eleveController->getEleve($clef_action[0]);
        // echo var_dump($valeur);
        $eleve->setClasse($valeur);
        echo var_dump($eleve);
        $datas[] = $eleveController->updateEleve($eleve);
      }
      if ($clef_action[1] == 'del') {
        $eleveController->del($clef_action[0]);
      }
    }
    // return new ApiResponse($data);
    return $this->wiki->redirect($redirectParams['eleveUpdate'], 200);
  }

  /**
  * @Route("/api/meeo/controle",methods={"POST"},options={"acl":{"public"}})
  */
  public function controle(Request $request) {
    $params = $this->getService(ParameterBagInterface::class);
    $redirectParams = $params->get('meeo')['redirect'];

    $noteManager = $this->getService(NoteManager::class);
    $content = $request->getContent();

    parse_str($content, $parameters);

    include 'tools/meeo/libs/controle/Controle.lib.php';

    $eleves = array_filter($parameters, function($key) {
      return strpos($key, 'eleve_') === 0;
    }, ARRAY_FILTER_USE_KEY);

    $controle = new Controle($parameters['intitule'], $parameters['matiere'], $parameters['max'], $parameters['coef'], $eleves);
    $controle->createNotes($noteManager);
    // return new ApiResponse($controle);
    return $this->wiki->redirect($redirectParams['controle'], 200);
  }

  /**
  * @Route("/api/meeo/absence",methods={"POST"},options={"acl":{"public"}})
  */
  public function absence(Request $request) {
    $params = $this->getService(ParameterBagInterface::class);
    $redirectParams = $params->get('meeo')['redirect'];
    $content = $request->getContent();
    parse_str($content, $parameters);

    // Créer l'absence dans bazar
    $eleve = $this->getService(EleveManager::class)->getEleve($parameters['eleve']);
    $absenceManager = $this->getService(AbsenceManager::class);
    $absence = $absenceManager->create($eleve, $parameters['date_debut_absence'], $parameters['date_fin_absence'], $parameters['justification']);

    // Récupération de l'expéditeur
    $expediteur = $this->getService(AuthController::class)->getLoggedUser();
    // echo "Expediteur : ".var_dump($expediteur)."\n";

    foreach ($eleve->getParentEmails() as $destinataire) {
      echo "Envoyer à : ".$destinataire."\n";

      // Génération du rendu du mail
      $body = $this->render("@meeo/eleve_creer_absence.twig", [
        'eleve' => $eleve,
        'absence' => $parameters
      ]);
      // echo $body;

      // Envoi de mail à la personne qui a été absente
      if (!send_mail($expediteur['email'], $expediteur['name'], $destinataire, "Absence", $body, $body)) 
        echo "Erreur d'envoie de mail";
    }

    // return new ApiResponse($parameters);
    return $this->wiki->redirect($redirectParams['absence'], 200);
  }
}