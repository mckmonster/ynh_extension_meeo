<?php

use YesWiki\Core\YesWikiAction;
use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Core\Service\UserManager;
use YesWiki\Meeo\Service\EleveManager;

class AbsencesAction extends YesWikiAction
{
    public function formatArguments($arg)
    {
        return [];
    }

    public function run()
    {
        $meeo_config = $this->params->get('meeo');
        $formId = $meeo_config['absencesFormId'];
        $eleveIdentifier = 'listefiche'.$meeo_config['elevesFormId'].'bf_eleve';
        $groupeEE = $meeo_config['groupeEE'];

        $entryManager = $this->getService(EntryManager::class);
        $userManager = $this->getService(UserManager::class);
        $elevemanager = $this->getService(EleveManager::class);

        if ( $userManager->isInGroup($groupeEE, admincheck: false) ) {
            $entries = $entryManager->search(['formsIds' => $formId]);
            $grouped_entries = [];

            foreach ($entries as $entry) {
                // echo "<p>".var_dump($entry)."</p>";
                $identifier = $entry[$eleveIdentifier];
                //echo "<p>".var_dump($identifier)."</p>";
                $eleve = $elevemanager->getEleve($identifier);
                // echo "<p>".var_dump($eleve)."</p>";
                $nom = $eleve->getNom();
                if (!isset($grouped_entries[$identifier])) {
                    $grouped_entries[$identifier] = [
                        'nom' => $nom,
                        'entries' => []
                    ];
                }
                $grouped_entries[$identifier]['entries'][] = $entry;
            }
            
            return $this->render('@meeo/edu_absences.twig', [
                "groups" => $grouped_entries,
            ]);
        } else {
            $username = $userManager->getLoggedUsername();
            $eleves = $elevemanager->getAll();
            foreach ($eleves as $eleve) {
                if ($eleve->getNom() == $username || $eleve->isParent($username)) {
                    $selectedEleve = $eleve;
                }
            }

            if ($selectedEleve != null) {
                $identifier = $selectedEleve->getId();
                // echo "<p>".var_dump($selectedEleve)."</p>";
                $entries = $entryManager->search(['formsIds' => $formId, 'queries' => [ $eleveIdentifier => $identifier]]);

                return $this->render('@meeo/eleve_absences.twig', [
                    "entries" => $entries,
                ]);
            }

            return "Utilisateur non connus : ". $username;
        }
    }
}
