<?php

namespace YesWiki\Meeo;

use YesWiki\Core\YesWikiAction;
use YesWiki\Meeo\Service\NoteManager;

class NotesAction extends YesWikiAction
{
    public function formatArguments($arg)
    {
        return [];
    }

    public function run()
    {
        try {
            $noteManager = $this->getService(NoteManager::class);
            return $noteManager->viewBulletins();
        } catch (Exception $e) {
            return 'Exception reçue : '.$e->getMessage()."\n";
        }
        
    }
}
