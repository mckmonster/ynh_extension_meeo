<?php

namespace YesWiki\Meeo;

use YesWiki\Core\YesWikiAction;
use YesWiki\Meeo\Service\NoteManager;

class ControleAction extends YesWikiAction
{
    public function formatArguments($arg)
    {
        return [];
    }

    public function run()
    {
        $noteManager = $this->getService(NoteManager::class);
        return $noteManager->viewControle();
    }
}