<?php

use YesWiki\Core\YesWikiAction;
use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Bazar\Service\ListManager;
use YesWiki\Meeo\Service\EleveManager;

class ElevesAction extends YesWikiAction
{
  public function formatArguments($arg)
  {
      return [];
  }

  public function run()
  {
    try {
      $eleveManager = $this->getService(EleveManager::class);
      return $eleveManager->view();
    } catch (Exception $e) {
      echo 'Exception reçue : '. $e->getMessage() ."\n";
    }
  }
}