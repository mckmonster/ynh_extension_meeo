<?php

use YesWiki\Core\YesWikiAction;
use YesWiki\Meeo\Service\EleveManager;

class CreerAbsenceAction extends YesWikiAction
{
    public function formatArguments($arg)
    {
        return [];
    }

    public function run()
    {
      $eleveManager = $this->getService(EleveManager::class);

      return $this->render('@meeo/edu_creer_absence.twig', ['eleves' => $eleveManager->getAll()]);
    }
}
