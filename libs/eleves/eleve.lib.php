<?php

namespace YesWiki\Meeo\Eleve;

include 'tools/meeo/libs/eleves/parent.lib.php';

use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Meeo\Eleve\ParentEleve;

class Eleve {
  private $data;
  private $user;
  private $parents;

  public function __construct(EntryManager $entryManager, $meeo_config, $data) {
    $this->data = $data;
    // echo "<p>".var_dump($data)."</p>";
    $yunoshostFormId = $meeo_config['yunohostFormId'];
    $parentsFormId = $meeo_config['parentsFormId'];
    $elevesFormId = $meeo_config['elevesFormId'];
    // echo "<p>Config : ".var_dump($meeo_config)."</p>";
    $userId = $data['listefiche'.$yunoshostFormId.'bf_nom'];
    $this->user = $entryManager->getOne($userId);
    // echo "<p>".var_dump($userId)."</p>";
    $this->parents = [];
    $parents = $entryManager->search(['formsIds' => $parentsFormId, 'queries' => ['listefiche'.$elevesFormId.'bf_eleve' => $userId.'2']]);
    // $parents = $entryManager->search(['formsIds' => $parentsFormId]);
    // echo "<p>Parents : ".var_dump($parents)."</p>";
    foreach ( $parents as $parent) {
      array_push($this->parents, new ParentEleve($entryManager, $yunoshostFormId, $parent));
    }
    // echo "<p>".var_dump($this)."</p>";
  }

  public function getId() {
    return $this->data['id_fiche'];
  }

  public function getUserId() {
    return $this->user['bf_titre'];
  }

  public function getNom() {
    if (empty($this->user['bf_nom'])) {
      return $this->user['bf_titre'];
    } else {
      return $this->user['bf_nom'];
    }
  }

  public function getClasse() {
    if (empty($this->data['listeListeClassesbf_classe'])) {
      return $this->data['bf_classe'];
    }
    return $this->data['listeListeClassesbf_classe'];
  }

  public function setClasse(string $classe) {
    $this->data['listeListeClassesbf_classe'] = $classe;
  }

  public function getEmail() {
    return $this->user['bf_mail'];
  }

  public function getParentEmails() {
    $emails = [];
    foreach ($this->parents as $parent) {
      array_push($emails, $parent->getEmail());
    }
    return $emails;
  }

  public function getData() {
    return $this->data;
  }

  public function isParent($username) {
    // echo "<p>username :".$username."</p>";
    foreach ($this->parents as $parent) {
      // echo "<p>parent :".$parent->getUserId()."</p>";
      if ($parent->getUserId() == $username) {
        return true;
      }
    }
  }
}