<?php

namespace YesWiki\Meeo\Eleve;

use YesWiki\Bazar\Service\EntryManager;

class ParentEleve {
  private $data;
  private $user;

  public function __construct(EntryManager $entryManager, $yunoshostFormId, $data) {
    $this->data = $data;
    // echo "<p>".var_dump($data)."</p>";
    $userId = $data['listefiche'.$yunoshostFormId.'bf_parent'];
    $this->user = $entryManager->getOne($userId);
    // echo "<p>".var_dump($userId)."</p>";
    // echo "<p>".var_dump($this)."</p>";
  }

  public function getUserId() {
    return $this->user['bf_titre'];
  }

  public function getNom() {
    if (empty($this->user['bf_nom'])) {
      return $this->user['bf_titre'];
    } else {
      return $this->user['bf_nom'];
    }
  }

  public function getEmail() {
    return $this->user['bf_mail'];
  }
}