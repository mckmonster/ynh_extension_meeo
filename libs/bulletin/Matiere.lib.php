<?php

namespace YesWiki\Meeo\Bulletin;

include 'tools/meeo/libs/bulletin/Note.lib.php';

use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Meeo\Service\NoteManager;

class Matiere {
  private $id;
  private $nom;
  private $notes;

  public function __construct($wiki, Bulletin $bulletin, string $id, string $nom) {
    $this->id = $id;
    $this->nom = $nom;

    $eleveId = $bulletin->getId();
    // echo "<p>".$eleveId."</p>";

    $noteManager = $wiki->services->get(NoteManager::class);
    $entries = $noteManager->getNotes($eleveId, $id);

    // echo "<p>".var_dump($entries)."</p>";
    foreach ($entries as $entry) {
      if (!empty($entry['bf_note']))
      $this->notes[] = new Note($entry['bf_intitule'], $entry['bf_note'], $entry['listeListeNotemaxbf_notemax'], $entry['bf_coef']);
    }
  }

  public function getId() {
    return $this->id;
  }

  public function getNom() {
    return $this->nom;
  }

  public function getMoyenne() {
    $total = 0;
    $totalCoef = 0.0;
    foreach ($this->notes as $note) {
      $total += $note->getNoteOn20() * $note->getCoef();
      $totalCoef += $note->getCoef();
    }
    return $total / $totalCoef;
  }

  public function getNotes() {
    return $this->notes;
  }

  public function isVisible() {
    return $this->notes;
  }
}