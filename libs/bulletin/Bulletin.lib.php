<?php

namespace YesWiki\Meeo\Bulletin;

include 'tools/meeo/libs/bulletin/Matiere.lib.php';

use YesWiki\Bazar\Service\ListManager;
use YesWiki\Meeo\Eleve\Eleve;

class Bulletin {
  private $eleve;
  private $matieres = [];

  public function __construct($wiki, Eleve $eleve) {
    $this->eleve = $eleve;
    // echo var_dump($eleve);

    $listManager = $wiki->services->get(ListManager::class);
    $matieres = $listManager->getOne('ListeMatiere');
    // echo "<p>".var_dump($matieres)."</p>";
    foreach ($matieres['label'] as $id => $matiere) {
      // echo "<p>".$id."</pd>";
      $this->matieres[] = new Matiere($wiki, $this, $id, $matiere);
    }
  }

  public function getId() {
    return $this->eleve->getId();
  }

  public function getNom() {
    return $this->eleve->getNom();
  }

  public function getClasse() {
    return $this->eleve->getClasse();
  }

  public function getMoyenne() {
    $total = 0;
    $totalMatieres = 0;
    foreach ( $this->matieres as $matiere) {
      if ($matiere->isVisible()) {
        $total += $matiere->getMoyenne();
        ++$totalMatieres;
      }
    }
    return $total / $totalMatieres;
  }

  public function getMatieres() {
    return $this->matieres;
  }

  public function isVisible() {
    foreach ($this->matieres as $matiere) {
      if ( $matiere->isVisible() ) {
        return true;
      }
      return false;
    }
  }
}