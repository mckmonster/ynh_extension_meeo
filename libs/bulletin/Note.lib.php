<?php

namespace YesWiki\Meeo\Bulletin;

class Note {
  private $intitule;
  private $note;
  private $coef;
  private $max;

  public function __construct(string $intitule, float $note, float $max, float $coef = 1.0) {
    $this->intitule = $intitule;
    $this->note = $note;
    $this->coef = $coef;
    $this->max = $max;
  }

  public function getIntitule() {
    return $this->intitule;
  }

  public function getNote() {
    return $this->note;
  }

  public function getNoteOn20() {
    return ($this->note / $this->max) * 20.0;
  }

  public function getCoef() {
    return $this->coef;
  }

  public function getMax() {
    return $this->max;
  }
}