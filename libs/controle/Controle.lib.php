<?php

namespace YesWiki\MeeO\Controle;

use YesWiki\Meeo\Service\NoteManager;

class Controle {
  public $intitule;
  public $matiere;
  public $max;
  public $coef;
  public $eleves;

  public function __construct($intitule, $matiere, $max, $coef, $eleves) {
    $this->intitule = $intitule;
    $this->matiere = $matiere;
    $this->max = $max;
    $this->coef = $coef;
    $this->eleves = $this->removePrefixFromKeys($eleves, "eleve_");
  }

  function removePrefixFromKeys($inputArray, $prefix) {
    $outputArray = array();
    foreach ($inputArray as $key => $value) {
        if (strpos($key, $prefix) === 0) {
            $newKey = substr($key, strlen($prefix)); // Remove the prefix
            $outputArray[$newKey] = $value;
        } else {
            $outputArray[$key] = $value;
        }
    }
    return $outputArray;
  }

  public function createNotes(NoteManager $noteManager) {
    foreach ($this->eleves as $eleve => $note) {
      if (!empty($note))
        $noteManager->createNote($this->intitule, $eleve, $this->matiere, $note, $this->max, $this->coef);
    };
  }
}