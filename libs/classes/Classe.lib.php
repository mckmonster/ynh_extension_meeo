<?php

namespace YesWiki\Meeo\Classes;

use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Meeo\Service\NoteManager;

class Classe {
    private $id;
    private $name;
    private $bulletins = [];

    public function __construct($wiki, $id, $name) {
        $this->id = $id;
        $this->name = $name;

        $noteManager = $wiki->services->get(NoteManager::class);
        foreach ($noteManager->allBulletins() as $bulletin) {
            if ($bulletin->getClasse() == $this->id) {
                array_push($this->bulletins, $bulletin);
            }
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getBulletins() {
        return $this->bulletins;
    }

    public function haveBulletins(){
        foreach ($this->bulletins as $bulletin) {
            if ( $bulletin->isVisible() ) {
              return true;
            }
        }
        return false;
    }
}