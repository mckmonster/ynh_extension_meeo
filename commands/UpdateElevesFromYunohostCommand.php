<?php

namespace YesWiki\Meeo\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use YesWiki\Wiki;
use YesWiki\Bazar\Service\EntryManager;

class UpdateElevesFromYunohostCommand extends Command {
  protected $wiki;

  public function __construct(Wiki &$wiki) {
      parent::__construct();
      $this->wiki = $wiki;
  }

  protected function configure() {
        $this
            // the name of the command : ./yeswicli helloworld:hello"
            ->setName('meeo:update')
            // the short description shown while running "./yeswicli list"
            ->setDescription('Update la liste des élèves depuis les utilisateurs Yunohost')

            // the full command description shown when running the command with
            // the "--help" option
            // ->setHelp("This command display the message \"Hello !\" with options for uppercase of add a username.\n" .
            //     "The argument \"username\" can be used to add a username. Example : \n" .
            //     "Command line'./yeswicli helloworld:hello \"John Smith\"' gives \"Hello John Smith !\"")

            // add argument for username
            // second parameter could be InputArgument::OPTIONAL <=> null, InputArgument::REQUIRED, InputArgument::IS_ARRAY
            // third parameter is the description
            // forth parameter is default value
            // ->addArgument('username', InputArgument::OPTIONAL, 'Username')

            // add option to display output as UPPERCASE
            // second parameter null|string is shortcut
            // third parameter null|int could be InputOption::VALUE_NONE <=> null, InputOption::VALUE_REQUIRED
            //          , InputOption::VALUE_OPTIONAL, InputOption::VALUE_IS_ARRAY, InputOption::VALUE_NEGATABLE
            // forth parameter is the description
            // ->addOption('uppercase', 'u', InputOption::VALUE_NONE, 'Display output in UPPERCASE')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
      $entryManager = $this->wiki->services->get(EntryManager::class);
      $params = $this->wiki->services->get(ParameterBagInterface::class);
      $meeo_config = $params->get('meeo');
      $yunohostFormId = $meeo_config['yunohostFormId'];
      $elevesFormId = $meeo_config['elevesFormId'];

      $yunohostEntries = $entryManager->search(['formsIds' => $yunohostFormId]);
      // echo var_dump($yunohostEntries)."\n";
      foreach ($yunohostEntries as $yunohostEntry) {
        // echo var_dump($yunohostEntry)."\n";
        $eleveEntry = $entryManager->search(['formsIds' => $elevesFormId, 'queries' => [
          'bf_titre' => $yunohostEntry['bf_titre']
        ]]);
        if ($eleveEntry == null) {
          $entryManager->create($elevesFormId, [
            'bf_titre' => $yunohostEntry['id_fiche'],
            'listefiche'.$yunohostFormId.'Listebf_nom' => $yunohostEntry['id_fiche'],
            'antispam' => 1
            ]);
        }
      }

      return Command::SUCCESS;
    }
}