<?php

namespace YesWiki\Meeo\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use YesWiki\Wiki;
use YesWiki\Bazar\Service\FormManager;
use YesWiki\Core\Service\PageManager;
use YesWiki\Core\Service\TripleStore;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CreateFormsCommand extends Command
{
    protected $wiki;
    protected $absenceFormId;
    protected $notesFormId;
    protected $eleveFormId;
    protected $yunohostFormId;

    public function __construct(Wiki &$wiki)
    {
        parent::__construct();
        $this->wiki = $wiki;
        $params = $this->wiki->services->get(ParameterBagInterface::class);
        $meeo_config = $params->get('meeo');
        $this->absenceFormId = $meeo_config['absencesFormId'];
        $this->notesFormId = $meeo_config['notesFormId'];
        $this->eleveFormId = $meeo_config['elevesFormId'];
        $this->yunohostFormId = $meeo_config['yunohostFormId'];
    }

    protected function configure()
    {
        $this
            // the name of the command : ./yeswicli helloworld:hello"
            ->setName('meeo:createforms')
            // the short description shown while running "./yeswicli list"
            ->setDescription('Permet de créer les formulaires pour ')

            // the full command description shown when running the command with
            // the "--help" option
            // ->setHelp("This command display the message \"Hello !\" with options for uppercase of add a username.\n" .
            //     "The argument \"username\" can be used to add a username. Example : \n" .
            //     "Command line'./yeswicli helloworld:hello \"John Smith\"' gives \"Hello John Smith !\"")

            // add argument for username
            // second parameter could be InputArgument::OPTIONAL <=> null, InputArgument::REQUIRED, InputArgument::IS_ARRAY
            // third parameter is the description
            // forth parameter is default value
            // ->addArgument('username', InputArgument::OPTIONAL, 'Username')

            // add option to display output as UPPERCASE
            // second parameter null|string is shortcut
            // third parameter null|int could be InputOption::VALUE_NONE <=> null, InputOption::VALUE_REQUIRED
            //          , InputOption::VALUE_OPTIONAL, InputOption::VALUE_IS_ARRAY, InputOption::VALUE_NEGATABLE
            // forth parameter is the description
            // ->addOption('uppercase', 'u', InputOption::VALUE_NONE, 'Display output in UPPERCASE')
        ;
    }

    private function createOrUpdate($formManager, $formId, $data) {
        echo $formId.": ".$data['bn_label_nature'];
        $existingForm = $formManager->getOne($formId);
        if ($existingForm)
            return $formManager->update($data);
        
        return $formManager->create($data);
    }

    private function absencesForm(FormManager $formManager, $meeo_config)
    {
        $data = [
            "bn_id_nature" => $this->absenceFormId,
            "bn_label_nature" =>  "Absences",
            "bn_description" =>  "Absences des élèves",
            "bn_condition" =>  "",
            "bn_sem_context" =>  "",
            "bn_sem_type" =>  "",
            "bn_sem_use_template" =>  "1",
            "bn_template" =>  <<<EOT
titre***Absence : {{bf_eleve}} - {{bf_date_debut}}***Titre Automatique***
listefiche***$this->eleveFormId***Elève*** *** *** ***bf_eleve*** ***1*** *** *** * *** * *** *** *** ***
listedatedeb***bf_date_debut***Debut*** *** ***today*** *** ***1*** *** *** * *** * *** *** *** ***
listedatedeb***bf_date_fin***Fin*** *** ***today*** *** ***1*** *** *** * *** * *** *** *** ***
textelong***bf_commentaire***Commentaire*** *** *** *** ***wiki***0*** *** *** * *** * *** *** *** ***
EOT,
            "bn_ce_i18n" =>  "fr-FR",
            "bn_only_one_entry" =>  "N",
            "bn_only_one_entry_message" =>  null
        ];

        $this->createOrUpdate($formManager, $this->absenceFormId, $data);
    }

    private function matiereList()
    {
        $pageManager = $this->wiki->services->get(PageManager::class);
        $tripleStore = $this->wiki->services->get(TripleStore::class);

        if (!$pageManager->getOne('ListeMatiere')) {
            $pageManager->save('ListeMatiere', '{"label":{"francais":"Français","math":"Math"},"titre_liste":"Matieres"}');
            // in case, there is already some triples for 'ListOuinonLms', delete them
            $tripleStore->delete('ListeMatiere', 'http://outils-reseaux.org/_vocabulary/type', null);
            // create the triple to specify this page is a list
            $tripleStore->create('ListeMatiere', 'http://outils-reseaux.org/_vocabulary/type', 'liste', '', '');
        }
    }

    private function notesForm(FormManager $formManager, $meeo_config)
    {
        $this->matiereList();

        $data = [
            "bn_id_nature" => $this->notesFormId,
            "bn_label_nature" =>  "Notes",
            "bn_description" =>  "Notes des élèves",
            "bn_condition" =>  "",
            "bn_sem_context" =>  "",
            "bn_sem_type" =>  "",
            "bn_sem_use_template" =>  "1",
            "bn_template" =>  <<<EOT
titre***Note : {{bf_intitule}} - {{bf_eleve}} - {{bf_matiere}}***Titre Automatique***
texte***bf_intitule***Intitulé*** *** *** *** ***text***1*** *** *** * *** * *** *** *** ***
liste***ListeMatiere***Matière*** *** *** ***bf_matiere*** ***1*** *** *** * *** * *** *** *** ***
listefiche***$this->eleveFormId***Elève*** *** *** ***bf_eleve*** ***1*** *** *** * *** * *** *** *** ***
texte***bf_note***Note***0***20*** *** ***number***1*** *** *** * *** * *** *** *** ***
texte***bf_coef***Coefficient***0.5***10***1*** ***number***0*** *** *** * *** * *** *** *** ***
EOT,
            "bn_ce_i18n" =>  "fr-FR",
            "bn_only_one_entry" =>  "N",
            "bn_only_one_entry_message" =>  null
        ];

        $this->createOrUpdate($formManager, $this->notesFormId, $data);
    }

    private function classeList()
    {
        $pageManager = $this->wiki->services->get(PageManager::class);
        $tripleStore = $this->wiki->services->get(TripleStore::class);

        if (!$pageManager->getOne('ListeClasses')) {
            $pageManager->save('ListeClasses', '{"label":{},"titre_liste":"Classe"}');
            // in case, there is already some triples for 'ListOuinonLms', delete them
            $tripleStore->delete('ListeClasses', 'http://outils-reseaux.org/_vocabulary/type', null);
            // create the triple to specify this page is a list
            $tripleStore->create('ListeClasses', 'http://outils-reseaux.org/_vocabulary/type', 'liste', '', '');
        }
    }

    private function elevesForms(FormManager $formManager, $meeo_config) {
        $this->classeList();

        $data = [
            "bn_id_nature" => $this->eleveFormId,
            "bn_label_nature" =>  "Eleves",
            "bn_description" =>  "Liste des élèves",
            "bn_condition" =>  "",
            "bn_sem_context" =>  "",
            "bn_sem_type" =>  "",
            "bn_sem_use_template" =>  "1",
            "bn_template" =>  <<<EOT
titre***{{ bf_nom }} - {{ bf_classe }}***Titre Automatique***
listefiche***$this->yunohostFormId***Liste déroulante*** *** *** ***bf_nom*** ***1*** *** *** * *** * *** *** *** ***
liste***ListeClasses***Classe*** *** ***aucune***bf_classe*** ***1*** *** *** * *** * *** *** *** ***
EOT,
            "bn_ce_i18n" =>  "fr-FR",
            "bn_only_one_entry" =>  "N",
            "bn_only_one_entry_message" =>  null
        ];

        $this->createOrUpdate($formManager, $this->eleveFormId, $data);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $formManager = $this->wiki->services->get(FormManager::class);
        $params = $this->wiki->services->get(ParameterBagInterface::class);
        $meeo_config = $params->get('meeo');

        $this->absencesForm($formManager, $meeo_config);

        $this->notesForm($formManager, $meeo_config);

        $this->elevesForms($formManager, $meeo_config);

        echo 'Succeed\n';
        return Command::SUCCESS;
    }
}
