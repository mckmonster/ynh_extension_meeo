<?php

namespace YesWiki\Meeo\Service;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Bazar\Service\ListManager;
use YesWiki\Core\Controller\AuthController;
use YesWiki\Core\Service\UserManager;
use YesWiki\Wiki;
use YesWiki\Meeo\Bulletin\Bulletin;
use YesWiki\Meeo\Service\EleveManager;
use YesWiki\Meeo\Classes\Classe;

class NoteManager {
  protected $wiki;
  protected $params;
  protected $entryManager;
  protected $listManager;
  protected $formId;
  protected $eleveFormId;
  protected $eeGroup;
  protected $authController;
  protected $userManager;

  public function __construct( Wiki $wiki, ParameterBagInterface $params, EntryManager $entryManager, ListManager $listManager, AuthController $authController, UserManager $userManager)
  {
    $this->wiki = $wiki;
    $this->params = $params;
    $this->entryManager = $entryManager;
    $this->listManager = $listManager;
    $this->authController = $authController;
    $this->userManager = $userManager;

    $meeo_config = $this->params->get('meeo');
    $this->formId = $meeo_config['notesFormId'];
    $this->eleveFormId = $meeo_config['elevesFormId'];
    $this->eeGroup = $meeo_config['groupeEE'];
  }

  public function allBulletins() {
    $eleveManager = $this->wiki->services->get(EleveManager::class);
    $eleves = $eleveManager->getAll();

    $bulletins = [];
    foreach ( $eleves as $eleve) {
      $bulletins[] = new Bulletin($this->wiki, $eleve);
    }
    return $bulletins;
  }

  public function viewBulletins() {
    $eleveManager = $this->wiki->services->get(EleveManager::class);

    include 'tools/meeo/libs/bulletin/Bulletin.lib.php';
    include 'tools/meeo/libs/classes/Classe.lib.php';

    $loggedUser = $this->authController->getLoggedUser();
    
    // echo "<p>".var_dump($loggedUser)."</p>";
    if ($this->userManager->isInGroup($this->eeGroup, $loggedUser['name'], false)) {
      // echo "<p>".var_dump($eleves)."</p>";
      // echo "<p>".var_dump($bulletins)."</p>";
      $classes = [];
      $listClasse = $this->listManager->getOne('ListeClasses');
      foreach ($listClasse['label'] as $id => $classe) {
        array_push($classes, new Classe($this->wiki, $id, $classe));
      }
      // echo "<p>".var_dump($classes)."</p>";
      return $this->wiki->render('@meeo/edu_notes.twig', [
          "classes" => $classes,
          "opened" => ""
      ]);
    } else {
    //   echo "Eleve";
      $currentEleve = $eleveManager->getCurrentEleve($loggedUser['name']);
      if ($currentEleve != null) {
        $bulletin = new Bulletin($this->wiki, $currentEleve);
        return $this->wiki->render('@meeo/eleve_notes.twig', [
            "bulletin" => $bulletin,
            "opened" => "in"
        ]);
      }
    }
  }

  public function viewControle() {
    $eleves = $this->wiki->services->get(EleveManager::class)->getAll();
    // echo var_dump($eleves);
    $matieres = $this->listManager->getOne('ListeMatiere');
    // echo var_dump($matieres);
    $classes = $this->listManager->getOne('ListeClasses');
    $notesMax = $this->listManager->getOne('ListeNoteMax');
    // echo var_dump($classes);
    return $this->wiki->render('@meeo/edu_controle.twig', [
      'matieres' => $matieres['label'],
      'classes' => $classes['label'],
      'notesMax' => $notesMax['label'],
      'eleves' => $eleves,
    ]);
  }

  public function createNote($intitule, $eleve, $matiere, $note, $max, $coef) {
    if ($note == 0)
      return;

    $data = [
      'bf_titre' => "Note : ".$intitule." - ".$eleve." - ".$matiere,
      'bf_intitule' => $intitule,
      'listeListeMatierebf_matiere' => $matiere,
      'listeListeNotemaxbf_notemax' => $max,
      'listefiche'.$this->eleveFormId.'bf_eleve' => $eleve,
      'bf_note' => $note,
      'bf_coef' => $coef,
      'antispam' => true
    ];
    $this->entryManager->create($this->formId, $data);
  }

  public function getNotes($eleveId, $matiereId) {
    // echo "<p>".$eleveId."</p>";
    // echo "<p>".$matiereId."</p>";
    $entries = $this->entryManager->search(['formsIds' => $this->formId, 
      'queries' =>[
        'listefiche'.$this->eleveFormId.'bf_eleve' => \strtolower($eleveId),
        'listeListeMatierebf_matiere' => $matiereId
      ]
    ]);
    // echo "<p>".var_dump($entries)."</p>";
    return $entries;
  }
}
