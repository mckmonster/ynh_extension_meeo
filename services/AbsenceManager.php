<?php

namespace YesWiki\Meeo\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use YesWiki\Bazar\Service\EntryManager;
use YesWiki\Wiki;

use YesWiki\Meeo\Eleve\Eleve;

class AbsenceManager {
  private $wiki;
  private $params;
  private $entryManager;
  private $formId;
  private $eleveFormId;

  public function __construct( Wiki $wiki, ParameterBagInterface $params, EntryManager $entryManager) {
    $this->wiki = $wiki;
    $this->params = $params;
    $this->entryManager = $entryManager;

    $this->formId = $params->get('meeo')['absencesFormId'];
    $this->eleveFormId = $params->get('meeo')['elevesFormId'];
  }

  public function create(Eleve $eleve, $debut, $fin, $justification) {
    $data = [
      'bf_titre' => 'Absence : '.$eleve->getId().' - '.$debut,
      'listefiche'.$this->eleveFormId.'bf_eleve' => $eleve->getId(),
      'bf_date_debut' => $debut,
      'bf_date_fin' => $fin, 
      'bf_commentaire' => $justification,
      'antispam' => true
    ];

    // echo var_dump($data);
    return $this->entryManager->create($this->formId, $data);
  }
}