function filterClasse() {
  var value = $("#classe").val().toLowerCase();
  $("#eleves div.form-group").filter(function() {
    var text = $(this).attr('classe').toLowerCase();
    $(this).toggle(text.indexOf(value) > -1);
  });
}

jQuery(function(){
  $("#classe").on("change", function() {
    filterClasse();
  });

  filterClasse();
});
